var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

  var path = require('path');


  var requestjson = require('request-json')

  var urlMlabRaiz = "https://api.mlab.com/api/1/databases/lgonzalez/collections";
  var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var clienteMLabRaiz;
  var usuarioMLabRaiz;

  var urlClientes = "https://api.mlab.com/api/1/databases/lgonzalez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

  var clienteMLab = requestjson.createClient(urlClientes)

  var urlDocumentos = "https://api.mlab.com/api/1/databases/lgonzalez/collections/Documentos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

  var documentoMLab = requestjson.createClient(urlDocumentos)

  var urlTipoCambio="https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/2020-07-01/2020-08-31?token=d5edb10bc10694c1a5a256c8f32812bcbb9c85ba008cdd270aba8a1030f0284c";
  //var urlTipoCambio="https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF1,SF5,SF9?token=c273885b5e0bc7efa63cf88707b2e41241a194120e9cb32831b82af63f026ca7";

  var tipoCambio = requestjson.createClient(urlTipoCambio)

  var urlUsers = "https://api.mlab.com/api/1/databases/lgonzalez/collections/Users?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

  var usuarioMLab = requestjson.createClient(urlUsers)

  app.listen(port);


var bodyparser = require('body-parser')

app.use(bodyparser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

var movimientosJSON = require('./movimientosv2.json');

//console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
})
app.post('/', function(req, res) {
  res.send("Hemos recibido su petición post");
})

app.put('/', function(req, res) {
  res.send("Hemos recibido su petición put");
})

app.delete('/', function(req, res) {
  res.send("Hemos recibido su petición delete");
})

app.get('/Clientes/:idcliente', function(req, res) {
  res.send("Aquí tiene al cliente número " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function(req, res) {
  //console.log(req.params.id);
  res.send(movimientosJSON[req.params.id - 1]);
})

app.get('/v2/MovimientosQuery', function(req, res) {
  //console.log(req.query);
  res.send("Se recibio el query");
})

app.post('/v2/Movimientos', function(req, res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get('/Clientes', function(req, res) {
  clienteMLab.get('', function(err, resM, body) {
    if (err) {
      //console.log(body)
    } else {
      res.send(body);
    }
  })
})

app.post('/Clientes', function(req, res) {
  clienteMLab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })
})

app.post('/Login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"correo":"'+ email +'","password":"' +password + '"}'
  //var query = `q={"email":"${email}","password":"${password}"}`
  //console.log(query)
  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  //console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logado')
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

/// Inserta usuario en la colleción usuario
app.post('/InsertData', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var usuario = req.body.nombre;
  var apellido = req.body.apellido;
  var correo = req.body.correo;
  var password = req.body.password;
  //console.log("datos req: "+JSON.stringify(req.body));
  //console.log("NOMBRE: "+usuario + " APELLIDO:"+ apellido + " CORREO: "+correo+ " PASSWORD: "+password)
  var urlMLab = urlMlabRaiz + "/Usuarios?&" + apiKey;
  usuarioMlabRaiz = requestjson.createClient(urlMLab)
  usuarioMlabRaiz.post('', req.body, function(err, resM, body) {
      //console.log("body: "+ body)
      res.send(body)

  })
})

//Consulta de documentos
app.get('/Documentos', function(req, res) {
  documentoMLab.get('', function(err, resM, body) {
    if (err) {
      //console.log(body)
    } else {
      res.send(body);
    }
  })
})

//Consulta tipo de cambio
app.get('/Cambios', function(req, res) {
  tipoCambio.get('', function(err, resM, body) {
    if (err) {
      //console.log(body)
    } else {
      res.send(body);
      //console.log(body)
    }
  })
})

/// Inserta usuario en la colleción documentos
app.post('/InsertDocuments', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var mes = req.body.mes;
  var contratos = req.body.contratos;
  var fatca = req.body.fatca;
  var kyc = req.body.kyc;
  var bcom = req.body.bcom;
  var bmovil = req.body.bmovil;
  var total = req.body.total;
  //console.log("Request Documentos: "+JSON.stringify(req.body));
  //console.log("Mes: "+mes + " Contratos:"+ contratos + "Fatca: "+fatca+ " KYC:"+kyc + " BCOM:"+bcom+" BMOVIL: "+bmovil+ " Total:"+ total)
  var urlMLab = urlMlabRaiz + "/Documentos?&" + apiKey;
  usuarioMlabRaiz = requestjson.createClient(urlMLab)
  usuarioMlabRaiz.post('', req.body, function(err, resM, body) {
      //console.log("body: "+ body)
      res.send(body)
  })
})
